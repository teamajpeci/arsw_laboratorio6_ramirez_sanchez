package edu.eci.arsw.stub;

import java.rmi.RemoteException;

import edu.eci.arsw.ScreenCaptureException;

public interface StubCapturePantalla extends java.rmi.Remote {
	
	public byte[] getCurrentScreenshot() throws RemoteException, ScreenCaptureException;

}
