package edu.eci.arsw.skeleton;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.imageio.ImageIO;

import edu.eci.arsw.ScreenCaptureException;
import edu.eci.arsw.stub.StubCapturePantalla;



public class CapturePimp extends UnicastRemoteObject implements StubCapturePantalla {

	public CapturePimp() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public byte[] getCurrentScreenshot() throws RemoteException, ScreenCaptureException {
		// TODO Auto-generated method stub
		try{
			Robot robot = new Robot();
	        Rectangle captureSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());		
	
	        BufferedImage screenshot = robot.createScreenCapture(captureSize);
	
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ImageIO.write( screenshot, "jpg", baos );
	        baos.flush();
	        byte[] imageInByte = baos.toByteArray();
	        baos.close();
	        
	        return imageInByte;
		} catch(AWTException e){
			throw new ScreenCaptureException("",e);
		} catch (IOException e) {
			throw new ScreenCaptureException("",e);
		}
		
	}
}

