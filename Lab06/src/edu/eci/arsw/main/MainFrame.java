package edu.eci.arsw.main;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import edu.eci.arsw.skeleton.CapturePimp;

public class MainFrame {

	/**
	 * @param args
	 * @throws RemoteException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws RemoteException, UnknownHostException {
		
		LocateRegistry.createRegistry(1111);
		Registry reg=LocateRegistry.getRegistry("10.2.67.11",1111);
		CapturePimp skeleton = new CapturePimp();
		reg.rebind("CapturePimp",skeleton);

	}

}
