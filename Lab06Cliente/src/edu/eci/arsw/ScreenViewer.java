package edu.eci.arsw;

import javax.imageio.ImageIO;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.*;

import javax.swing.*;

import edu.eci.arsw.stub.StubCapturePantalla;


import java.awt.Graphics;
 
/**
 * RMI LAB
 * This is the codebase of a RMI exercise. The objective is to implement a client/server remote desktop
 * viewer using RMI technology. 
 * @author hector.cadavid@escuelaing.edu.co
 *
 */
public class ScreenViewer{
 
	static Boolean stopAndExit=false;
	
	public static void main(String[] args) throws java.io.IOException, AWTException, InterruptedException, ScreenCaptureException, NotBoundException{
		 
		
		JFrame canvas = new JFrame();				
		canvas.setLayout(new BorderLayout());
		
		canvas.setSize(1280,800);
		canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		canvas.setTitle("Remote client");
		
		Container pane = canvas.getContentPane();
		ColorPanel panel = new ColorPanel();
				
		
		//ScreenCapturer sc=new ScreenCapturer();
		
		//Para obtener el acceso a un registro RMI remoto:
		Registry reg=LocateRegistry.getRegistry("10.2.67.11", 1111);
		//Para obtener un objeto remoto registrado en dicho registro (este método retorna
		//un proxy que puede tomar la forma del ‘stub’ de dicha clase):
		StubCapturePantalla sc = (StubCapturePantalla) reg.lookup("CapturePimp");

		
		byte[] screenshotBytes=sc.getCurrentScreenshot();		
		InputStream in = new ByteArrayInputStream(screenshotBytes);
		BufferedImage screenshot = ImageIO.read(in);
		
		panel.setScreenShot(screenshot);
		pane.add(panel, BorderLayout.CENTER);
		
		JButton activationButton=new JButton("Stop and exit");
		
		activationButton.addActionListener(
				new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						stopAndExit=true;
						
					}
				}
		);
		
		pane.add(activationButton,BorderLayout.NORTH);
		
		canvas.setVisible(true);
                
		while(!stopAndExit){

			screenshotBytes=sc.getCurrentScreenshot();		
			in = new ByteArrayInputStream(screenshotBytes);
			screenshot = ImageIO.read(in);
	        panel.setScreenShot(screenshot);
			panel.repaint();
			Thread.sleep(1000);
		}
		
		System.exit(0);
		
	}
}
 
class ColorPanel extends JComponent{
	BufferedImage screenShot;
	
	
	public void setScreenShot(BufferedImage screenShot) {
		this.screenShot = screenShot;
	}

	public ColorPanel(){

	}
 
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(screenShot, null, 0,0);
	}
}
